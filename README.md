# Corefact Development

All you need to start developing software for Corefact.  This repo relies on Docker to make sure that all programmers share the same development environment.

## Setup

### 1. Install Docker

#### Mac

Installing [Docker for Mac](https://docs.docker.com/docker-for-mac/install/) is all you need to do, it includes docker, docker-compose and docker-machine. Make sure that you are **not** installing [Docker toolbox](https://docs.docker.com/toolbox/overview/).

#### Ubuntu

Instalation in linux is more involved and depends on the distribution. In case of Ubuntu you need to download [Docker](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/) and [docker-compose](https://docs.docker.com/compose/install/) separately.

### 2. Setup repository

Clone this repository in your machine and initialize the `webserver` submodule.

```
$ git clone --recursive git@bitbucket.org:rfrm92/corefact_development.git
```

### 3. Download configuration files

Download [config_docker.zip](https://bitbucket.org/corefact/webserver/downloads/config_docker.zip) and extract the contents to `webserver/config`.

### 4. Download database dump

Download [01_dump.sql.gz](https://bitbucket.org/corefact/webserver/downloads/01_dump.sql.gz) and move it **as is** inside the `db/docker-entrypoint-initdb.d` directory.

### 5. Run migrations

```
$ docker-compose run --rm web "rake db:migrate"
```

### 6. Start the application

Just run

```
$ docker-compose up
```
This will take a few minutes the first time is executed because the database will need to import the dump to the `corefact_development` and `corefact_test`.

## Usefull commands

| Goal                       | Command
|----------------------------|---------------------------------------------------
| Open rails console         | `$ docker-compose run --rm web "bundle exec script/console"`
| Run tests                  | `$ docker-compose run --rm web "rake spec"`
| Run migrations             | `$ docker-compose run --rm web "rake db:migrate"` 
| Generate api documentation | `$ docker-compose run --rm web "rake api:v2:generate_docs"`
