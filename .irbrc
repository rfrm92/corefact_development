require 'irb/completion'
require 'irb/ext/save-history'

IRB.conf[:AUTO_INDENT]=true
IRB.conf[:HISTORY_FILE] = "#{ENV['APP_HOME']}/.irb-save-history"
IRB.conf[:SAVE_HISTORY] = 100
IRB.conf[:USE_READLINE] = true
