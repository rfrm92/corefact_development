"${mysql[@]}" <<-SQL
  CREATE DATABASE IF NOT EXISTS corefact_test;
SQL

gunzip -c /docker-entrypoint-initdb.d/01_dump.sql.gz | mysql -uroot -pdefaultpw corefact_test
