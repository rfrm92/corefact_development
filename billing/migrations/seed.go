package main

import (
	"corefact/billing/config"
	"corefact/billing/model"
	"fmt"

	"github.com/jinzhu/gorm"
)

func seedDB(tx *gorm.DB) error {
	if err := tx.Create(&model.Account{ReferenceID: 1, ReferenceType: "agent"}).Error; err != nil {
		return err
	}
	if err := tx.Create(&model.Account{ReferenceID: 4, ReferenceType: "brokerage"}).Error; err != nil {
		return err
	}
	return nil
}

func main() {
	if err := config.ReadConfigOnce(); err != nil {
		fmt.Println(err)
		return
	}

	if err := model.Init(); err != nil {
		fmt.Println("Error when connecting to DB")
		return
	}

	model.DB.LogMode(true)

	tx := model.DB.Begin()
	if err := tx.Error; err != nil {
		fmt.Println(err)
		return
	}
	if err := seedDB(tx); err != nil {
		tx.Rollback()
		fmt.Println("Rolled back due to error: ", err)
		return
	}

	if err := tx.Commit().Error; err != nil {

	}
	return
}
